#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define COULEURS 6
#define TAILLE 12

const int HEIGHT=250;
const int WIDTH=400;


void afficherEcran(SDL_Renderer *renderer,int indice_tab)
{
   // le renderer est créé
    SDL_Rect rect;
    /* couleur de fond */
    if (indice_tab==0)
    {
	    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    }
    if (indice_tab==1)
    {
	    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
    }
    if (indice_tab==2)
    {
	    SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);
    }
   	SDL_RenderClear(renderer);
    
    /* afficher à l'ecran */
    SDL_RenderPresent(renderer);
    
    
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    rect.x = 10;
    rect.y = 10;
    rect.w = 20;
    rect.h = 20;
    
    SDL_RenderFillRect(renderer,&rect);

    SDL_RenderPresent(renderer);
}

void createWindows(SDL_Window * tabWindow[],SDL_Renderer * tabRender[])
{
    int j;
    SDL_Window * window=NULL;
    SDL_Renderer *renderer=NULL;
    for(j=0;j<=2;j++)
    {
        window=SDL_CreateWindow("window",j*400,0,400,250,SDL_WINDOW_RESIZABLE);
        /*if (window == NULL)
        {
            SDL_log("Error : SDL fenetre %s\n", SDL_GetError());
        }*/
        tabWindow[j]=window;
        renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_SOFTWARE);
        /*if (renderer == NULL)
        {
            SDL_log("Error : SDL_Renderer fenetre %s\n", SDL_GetError());
        }*/
        tabRender[j]=renderer;
    }

}

int main ()
{
    int running=1;
    int height=HEIGHT;
    int width=WIDTH;

    int x;
    int y;

    SDL_Event event;
    SDL_Window *window1=SDL_CreateWindow("window",0,0,400,250,SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer1=SDL_CreateRenderer(window1,-1,SDL_RENDERER_SOFTWARE);
    SDL_Window *window2=SDL_CreateWindow("window",400,0,400,250,SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer2=SDL_CreateRenderer(window2,-1,SDL_RENDERER_SOFTWARE);

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    while (running) 
    {   
        while (SDL_PollEvent(&event))
        {  
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:  
                            printf("appui sur la croix\n");	
                            running = 0;
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
                            printf("Size : %d %d\n", width, height);
                        default:
                            afficherEcran(renderer1,1);
                            afficherEcran(renderer2,2);
                    }   
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    printf("Appui :%d %d\n", event.button.x, event.button.y);
                    if (event.button.x>10 && event.button.x<30 && event.button.y>10 && event.button.y<30)
                    {
                        SDL_GetWindowPosition(window1, &x, &y);
                        printf("Position init : %d %d\n", x, y);
                        if (y>=0)
                        {
                            SDL_SetWindowPosition(window1, x, y+1000);
                            SDL_SetWindowPosition(window2, x+400, y+1000);
                            while (y>0)
                            {
                                SDL_GetWindowPosition(window1, &x, &y);
                                SDL_SetWindowPosition(window1, x, y-2);
                                SDL_GetWindowPosition(window2, &x, &y);
                                SDL_SetWindowPosition(window2, x, y-4);
                                afficherEcran(renderer1,1);
                                afficherEcran(renderer2,2);
                            }
                            y=500;
                            while (y>0)
                            {
                                SDL_GetWindowPosition(window1, &x, &y);
                                SDL_SetWindowPosition(window1, x, y-2);
                                afficherEcran(renderer1,1);
                            }
                        }
                    }
                    afficherEcran(renderer1,1);
                    afficherEcran(renderer2,2);
                case SDL_MOUSEBUTTONUP:
                    afficherEcran(renderer1,1);
                    afficherEcran(renderer2,2);
                    break;
                case SDL_QUIT : 
                    printf("on quitte\n");    
                    running = 0;
            }
        
        }	
        SDL_Delay(1); //  delai minimal
    }
    
    
    SDL_DestroyRenderer(renderer1);
    SDL_DestroyRenderer(renderer2);
    SDL_DestroyWindow(window1);
    SDL_DestroyWindow(window2);
    
    SDL_Quit();
    return 0;
}
