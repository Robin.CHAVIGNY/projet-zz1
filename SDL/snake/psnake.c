#include <stdio.h>
#include <stdlib.h>


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

const int HEIGHT=750;
const int WIDTH=900;
/*
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer )
{
    SDL_Surface *my_image = NULL;          
    SDL_Texture* my_texture = NULL;   

    my_image = IMG_Load(file_image_name);  
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);
   
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}
*/


void afficherEcran(SDL_Renderer *renderer,int lvl,int n)
{
    SDL_Rect rect;

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderClear(renderer);
    if (lvl >0)
    {
        if(lvl==1)
        {
            int x= 450 -1.75*n;
            SDL_SetRenderDrawColor(renderer,0,0,0,255);
            rect.x = x;
            rect.y = 0;
            rect.w = (450-x)*2;
            rect.h = 750;
            SDL_RenderFillRect(renderer,&rect);
        }
        else if (lvl>1)
        {
            int x=450-1.75*100;
            SDL_SetRenderDrawColor(renderer,0,0,0,255);
            rect.x = x;
            rect.y = 0;
            rect.w = (450-x)*2;
            rect.h = 750;
            SDL_RenderFillRect(renderer,&rect);

            SDL_SetRenderDrawColor(renderer,125,100,0,0);
            rect.x = 375;
            rect.y = 400;
            rect.w = 100;
            rect.h = 75;
            SDL_RenderFillRect(renderer,&rect);
        }
    }
    else
    {
        SDL_RenderFillRect(renderer,&rect);
    }
}


int main()
{
    int running=1;
    int height=HEIGHT;
    int width=WIDTH;
    int lvl = 0;
    int n=0;
    int shoulddraw=0;



    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }
    SDL_Event event;
    SDL_Window *window=SDL_CreateWindow("window",500,180,900,750,SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_SOFTWARE);

   

     while (running) 
    {   
        while (SDL_PollEvent(&event))
        {  
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:  
                            printf("appui sur la croix\n");	
                            running = 0;
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
                            printf("Size : %d %d\n", width, height);
                            break;
                        default:
                            break;
                            
                    }   
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    printf("Appui :%d %d\n", event.button.x, event.button.y);
                    if (shoulddraw!=0)
                    {
                        if (n-1000<0)
                        {
                            n=0;
                        }
                        else
                        {
                            n=n-1000;
                        }
                        lvl=lvl-1;
                    }
                    shoulddraw=1;
                    
                    //n=0;
                    lvl +=1;
                    printf("lvl=%d\n",lvl);
                    //afficherEcran(renderer,lvl);
                    break;
                case SDL_MOUSEBUTTONUP:
                    //afficherEcran(renderer,lvl);
                    break;
                case SDL_QUIT : 
                    printf("on quitte\n");    
                    running = 0;
            }SDL_Rect
        source = {0}
        
        }	

        if (shoulddraw)
        {
            n+=1;
            if (n>999*(lvl))
            {
                shoulddraw=0;
            }
        }

        printf("n=%d\n",n);
        
        SDL_RenderClear(renderer);
        afficherEcran(renderer,lvl,n/10);
        SDL_RenderPresent(renderer);
        //n%=1001;
        SDL_Delay(1);
    }
    
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
