#include <stdio.h>
#include <stdlib.h>


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


const int HEIGHT=750;
const int WIDTH=900;

void background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Rect
        source = {0},
        window_dimensions = {0},
        destination = {0};
    
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}


void animation(SDL_Texture *my_texture, SDL_Texture *sprit, SDL_Window *window, SDL_Renderer *renderer)
{
    
    SDL_Rect window_dimensions = {0};
    SDL_Rect destination = {0}; // position dans la fenetre
    SDL_Rect state = {0};
    SDL_Rect source = {0}; // taille de la vignette 

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(sprit,NULL,NULL,&source.w,&source.h);

    int nb_images = 5;
    float zoom = 1;
    int offset_x = source.w / nb_images;
    int offset_y = source.h / 7 ;

    state.x = 0;
    
    state.w = offset_x;
    state.h = offset_y; 

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;
    destination.y = (window_dimensions.h - destination.h) /2;

    int speed = 9;

    for (int x = 0; x < window_dimensions.w -destination.w ; x += speed) 
    {   
        SDL_RenderClear(renderer); 
        background(my_texture,window,renderer);
        destination.x = window_dimensions.w -destination.w - x; 
        if ((x/speed)%10<5)
        {
            state.y = 4 * offset_y +5;
        }
        else 
        {
            state.y = 5 * offset_y +5;
        }
        state.x += offset_x;
        state.x %= source.w;

        SDL_RenderCopy(renderer, sprit, &state,&destination); 
        SDL_RenderPresent(renderer);
        SDL_Delay(100);
    }
    SDL_RenderClear(renderer);
}




int main()
{
    int running=1;
    int height=HEIGHT;
    int width=WIDTH;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    SDL_Event event;
    SDL_Window *window=SDL_CreateWindow("window",500,180,900,750,SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_SOFTWARE);
    SDL_Texture *sprit= IMG_LoadTexture(renderer, "./sprit.png");
    SDL_Texture *my_texture= IMG_LoadTexture(renderer, "./fond.jpg");

   
     while (running) 
    {   
        while (SDL_PollEvent(&event))
        {  
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:  
                            printf("appui sur la croix\n");	
                            running = 0;
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
                            printf("Size : %d %d\n", width, height);
                            break;
                        default:
                            break;
                            
                    }   
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    printf("Appui :%d %d\n", event.button.x, event.button.y);
                    break;
                case SDL_MOUSEBUTTONUP:
                    break;
                case SDL_QUIT : 
                    printf("on quitte\n");    
                    running = 0;
            }
        }	
        animation(my_texture,sprit,window,renderer);
    }
    
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyTexture(my_texture);
    SDL_DestroyTexture(sprit);
    IMG_Quit();
    SDL_Quit();
    return 0;
}
