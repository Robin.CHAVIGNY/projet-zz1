#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include "jeu.h"

Regle chargerRegle(FILE* flux)
{
    Regle regle;
    int action_value;
    fscanf(flux, " - [%d, %d, %d, %d] --> %d (%d)",
                    &regle.proie.pos,
                    &regle.proie.dis,
                    &regle.predateur.pos,
                    &regle.predateur.dis,
                    &action_value,
                    &regle.priorite);
    regle.action = action_value;
    return regle;
}

TabRegle chargerTabRegle(FILE* flux)
{
    TabRegle tabRegle;
    tabRegle.tab=malloc(nb_regle*sizeof(Regle));
    for(int k=0; k<nb_regle; k++)
    {
        Regle regle;
        regle=chargerRegle(flux);
        tabRegle.tab[k]=regle;
    }
    return tabRegle;
}

void afficherRegle(FILE* flux, Regle regle)
{
    fprintf(flux, "- [%d, %d, %d, %d] --> %d (%d)\n",
            regle.proie.pos, regle.proie.dis,
            regle.predateur.pos, regle.predateur.dis,
            regle.action, regle.priorite);
}

void afficherTabRegle(FILE* flux, TabRegle tabRegle)
{
    for(int k=0; k<nb_regle; k++)
    {
        afficherRegle(flux, tabRegle.tab[k]);
    }
}

void freeTabregle(TabRegle tabregle)
{
    free(tabregle.tab);
}

void background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Rect
        source = {0},
        window_dimensions = {0},
        destination = {0};
    
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

int **creat_world ()
{
    int** matrice = (int**)malloc(T * sizeof(int*));
    for (int i = 0; i < T; i++)    
    {
        matrice[i] = (int*)malloc(T * sizeof(int));
        for (int j = 0; j < T; j++) 
        {
            matrice[i][j] = 0;
        }
    }
    return matrice;
}

void free_world(int** matrice)
{
    for (int i = 0; i < T; i++) 
    {
        free(matrice[i]);
    }
    free(matrice);
}

void printMatrice(int** matrice)
{
    for (int i = 0; i < T; i++)
    {
        for (int j = 0; j < T; j++)
        {
            printf("%d ", matrice[i][j]);
        }
        printf("\n");
    }
}

void loc_proie(int **matrice)
{
    int e1=0;
    int e2=0;
    int e3=0;
    int e4=0;

    while (e1==0 || e2==0 || e3==0 || e4==0)
    {
        int kx = rand()%T;
        int ky = rand()%T;
        int hx = rand()%T;
        int hy = rand()%T;
        int lx = rand()%T;
        int ly = rand()%T;
        int px = rand()%T;
        int py = rand()%T;
        
        if (matrice[kx][ky]==0 && e1!=1 )
        {
            matrice[kx][ky]=1;
            e1=1;
        }
        if (matrice[hx][hy]==0 && e2!=1)
        {
            matrice[hx][hy]=1;
            e2=1;
        }
        if (matrice[lx][ly]==0 && e3!=1)
        {
            matrice[lx][ly]=1;
            e3=1;
        }
        if (matrice[px][py]==0 && e4!=1)
        {
            matrice[px][py]=1;
            e4=1;
        }
    }
}

void afficher_proie(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window)
{
    SDL_Rect proie= {0,0,51,82};
    SDL_Rect destination = {0,0,0,0};
    SDL_Rect window_dimensions= {0,0,0,0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for (int i =0; i<T;i++)
    {
        for (int j = 0; j<T ; j++)
        {
            if (world[j][i]==1)
            {
                destination.x=i*window_dimensions.w/T;
                destination.y=j*window_dimensions.h/T;
                destination.w=window_dimensions.w/T;
                destination.h=window_dimensions.h/T;
                SDL_RenderCopy(renderer,sprite,&proie,&destination);
            }
        }
    }
}

void trouver_proie (int **world,emplacement_t tabproie[])
{
    int e=0;

    for(int i=0; i<T;i++)
    {
        for(int j=0;j<T;j++)
        {
            if (world[j][i]==1)
            {
                tabproie[e].x=j;
                tabproie[e].y=i;
                e+=1;
            }
        }
    }
}

int nb_bon_voisin(int **world, int x, int y)
{
    int nb_voisin = 4;

    if (x-1<0)
    {
        nb_voisin=nb_voisin-1;
    }
    else
    {
        if (world[x-1][y]!=0)
        {
            nb_voisin=nb_voisin-1;
        }
    }

    if (y-1<0)
    {
        nb_voisin=nb_voisin-1;
    }
    else
    {
        if (world[x][y-1]!=0)
        {
            nb_voisin=nb_voisin-1;
        }
    }

    if (x+1>T-1)
    {
        nb_voisin=nb_voisin-1;
    }
    else
    {
        if (world[x+1][y]!=0)
        {
            nb_voisin=nb_voisin-1;
        }
    }   

    if (y+1>T-1)
    {
        nb_voisin=nb_voisin-1;
    }
    else
    {
        if (world[x][y+1]!=0)
        {
            nb_voisin=nb_voisin-1;
        }
    }
    return nb_voisin;
}

bool bon_voisin(int **world,int x,int y)
{
    bool v=false;

    if (x>=0 && x<T && y>=0 && y<T )
    {
        if (world[x][y]==0)
        {
            v=true;
        }
    }
    return v;
}

void deplacer_proie(int **world, int nb_proie)
{
    emplacement_t tabproie[nb_proie];
    trouver_proie(world,tabproie);
    int ale;
    int e=1;

    for (int i=0;i<nb_proie;i++)
    {
        e=1;
        if (tabproie[i].x-1>=0)
        {
            if (world[tabproie[i].x-1][tabproie[i].y]==2)
            {
                if (tabproie[i].x+1<T)
                {
                    if (world[tabproie[i].x+1][tabproie[i].y]==0)
                    {
                        if (e==1)
                        {
                            e=0;
                            world[tabproie[i].x][tabproie[i].y]=0;
                            world[tabproie[i].x+1][tabproie[i].y]=1;
                        }
                    }
                }
            }
        }
        if (tabproie[i].x+1<T)
        {
            if (world[tabproie[i].x+1][tabproie[i].y]==2)
            {
                if (tabproie[i].x-1>=0)
                {
                    if (world[tabproie[i].x-1][tabproie[i].y]==0)
                    {
                        if (e==1)
                        {
                            e=0;
                            world[tabproie[i].x][tabproie[i].y]=0;
                            world[tabproie[i].x-1][tabproie[i].y]=1;
                        }
                    }
                }
            }
        }
        if (tabproie[i].y-1>=0)
        {
            if (world[tabproie[i].x][tabproie[i].y-1]==2)
            {
                if (tabproie[i].y+1<T)
                {
                    if (world[tabproie[i].x][tabproie[i].y+1]==0)
                    {
                        if (e==1)
                        {
                            e=0;
                            world[tabproie[i].x][tabproie[i].y]=0;
                            world[tabproie[i].x][tabproie[i].y+1]=1;
                        }
                    }
                }
            }
        }
        if (tabproie[i].y+1<T)
        {
            if (world[tabproie[i].x][tabproie[i].y+1]==2)
            {
                if (tabproie[i].y-1>=0)
                {
                    if (world[tabproie[i].x][tabproie[i].y-1]==0)
                    {
                        if (e==1)
                        {
                            e=0;
                            world[tabproie[i].x][tabproie[i].y]=0;
                            world[tabproie[i].x][tabproie[i].y-1]=1;
                        }
                    }
                }
            }
        }
        if (nb_bon_voisin(world,tabproie[i].x,tabproie[i].y)!=0)
        {
            while (e==1)
            {
                ale = rand()%5;
                if (ale ==0 && bon_voisin(world,tabproie[i].x-1,tabproie[i].y))
                {
                    e=0;
                    world[tabproie[i].x][tabproie[i].y]=0;
                    world[tabproie[i].x-1][tabproie[i].y]=1;
                    break;
                }
                if (ale==1 && bon_voisin(world,tabproie[i].x+1,tabproie[i].y))
                {
                    e=0;
                    world[tabproie[i].x][tabproie[i].y]=0;
                    world[tabproie[i].x+1][tabproie[i].y]=1;
                    break;
                }
                if (ale==2 && bon_voisin(world,tabproie[i].x,tabproie[i].y-1))
                {
                    e=0;
                    world[tabproie[i].x][tabproie[i].y]=0;
                    world[tabproie[i].x][tabproie[i].y-1]=1;
                    break;
                }
                if (ale==3 && bon_voisin(world,tabproie[i].x,tabproie[i].y+1))
                {
                    e=0;
                    world[tabproie[i].x][tabproie[i].y]=0;
                    world[tabproie[i].x][tabproie[i].y+1]=1;
                    break;
                }
                if (ale==4)
                {
                    e=0;
                    break;
                }
            }
        }
    } 
}

bool voisin_pas_proie(int x,int y, int **matrice )
{
    bool v = true;
    for (int i=0; i<4; i++)
    {
        if (x-1>=0)
        {
            if(matrice[x-1][y]==1)
            {
                v=false;
            }
        }
        if (y-1>=0)
        {
            if(matrice[x][y-1]==1)
            {
                v=false;
            }
        }
        if (x+1<T)
        {
            if(matrice[x+1][y]==1)
            {
                v=false;
            }
        }
        if (y+1<T)
        {
            if(matrice[x][y+1]==1)
            {
                v=false;
            }
        }
    }
    return v;
}

void loc_predateur(int **matrice)
{
    int e1=0;
    int e2=0;
    int e3=0;
    int e4=0;
    int e5=0;
    int e6=0;
    int e7=0;
    int e8=0;
    int e9=0;
    int e10=0;
    int e11=0;

    while (e1==0 || e2==0 || e3==0 || e4==0 || e5==0 || e6==0 || e7==0 || e8==0 || e9==0 || e10==0 || e11==0 )
    {
        int kx = rand()%T;
        int ky = rand()%T;
        int hx = rand()%T;
        int hy = rand()%T;
        int lx = rand()%T;
        int ly = rand()%T;
        int px = rand()%T;
        int py = rand()%T;
        int mx = rand()%T;
        int my = rand()%T;
        int nx = rand()%T;
        int ny = rand()%T;
        int gx = rand()%T;
        int gy = rand()%T;
        int fx = rand()%T;
        int fy = rand()%T;
        int dx = rand()%T;
        int dy = rand()%T;
        int bx = rand()%T;
        int by = rand()%T;
        int tx = rand()%T;
        int ty = rand()%T;

        
        if (matrice[kx][ky]==0 && e1!=1 && voisin_pas_proie(kx,ky,matrice))
        {
            matrice[kx][ky]=2;
            e1=1;
        }
        if (matrice[hx][hy]==0 && e2!=1 && voisin_pas_proie(hx,hy,matrice))
        {
            matrice[hx][hy]=2;
            e2=1;
        }
        if (matrice[lx][ly]==0 && e3!=1 && voisin_pas_proie(lx,ly,matrice))
        {
            matrice[lx][ly]=2;
            e3=1;
        }
        if (matrice[px][py]==0 && e4!=1 && voisin_pas_proie(px,py,matrice))
        {
            matrice[px][py]=2;
            e4=1;
        }
        if (matrice[mx][my]==0 && e5!=1 && voisin_pas_proie(mx,my,matrice))
        {
            matrice[mx][my]=2;
            e5=1;
        }
        if (matrice[nx][ny]==0 && e6!=1 && voisin_pas_proie(nx,ny,matrice))
        {
            matrice[nx][ny]=2;
            e6=1;
        }
        if (matrice[gx][gy]==0 && e7!=1 && voisin_pas_proie(gx,gy,matrice))
        {
            matrice[gx][gy]=2;
            e7=1;
        }
        if (matrice[fx][fy]==0 && e8!=1 && voisin_pas_proie(fx,fy,matrice))
        {
            matrice[fx][fy]=2;
            e8=1;
        }
        if (matrice[dx][dy]==0 && e9!=1 && voisin_pas_proie(dx,dy,matrice))
        {
            matrice[dx][dy]=2;
            e9=1;
        }
        if (matrice[bx][by]==0 && e10!=1 && voisin_pas_proie(bx,by,matrice))
        {
            matrice[bx][by]=2;
            e10=1;
        }
        if (matrice[tx][ty]==0 && e11!=1 && voisin_pas_proie(tx,ty,matrice))
        {
            matrice[tx][ty]=2;
            e11=1;
        }
    }
}

void afficher_predateur(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window)
{
    SDL_Rect proie= {0,0,126,91};
    SDL_Rect destination = {0,0,0,0};
    SDL_Rect window_dimensions= {0,0,0,0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for (int i =0; i<T;i++)
    {
        for (int j = 0; j<T ; j++)
        {
            if (world[j][i]==2)
            {
                destination.x=i*window_dimensions.w/T;
                destination.y=j*window_dimensions.h/T;
                destination.w=window_dimensions.w/T;
                destination.h=window_dimensions.h/T;
                SDL_RenderCopy(renderer,sprite,&proie,&destination);
            }
        }
    }
}

void trouver_predateur (int **world, emplacement_t tabpredateur[])
{
    int e=0;

    for(int i=0; i<T;i++)
    {
        for(int j=0;j<T;j++)
        {
            if (world[j][i]==2)
            {
                tabpredateur[e].x=j;
                tabpredateur[e].y=i;
                e+=1;
            }
        }
    }
}

void deplacer_predateur(int **world,TabRegle tabregle,int nb_proie)
{
    emplacement_t tabpredateur[NB_LOUP];
    trouver_predateur(world,tabpredateur);
    emplacement_t tabproie[nb_proie];
    trouver_proie(world,tabproie);
    int M[NB_LOUP];
    
    move_predateur(tabpredateur,tabproie,M,tabregle,nb_proie);

    for (int i=0;i<NB_LOUP;i++)
    {
        if (M[i]==0 && bon_voisin(world,tabpredateur[i].x-1,tabpredateur[i].y))
        {
                
            world[tabpredateur[i].x][tabpredateur[i].y]=0;
            world[tabpredateur[i].x-1][tabpredateur[i].y]=2;
        }
        if(M[i]==1 && bon_voisin(world,tabpredateur[i].x,tabpredateur[i].y-1))
        {
            world[tabpredateur[i].x][tabpredateur[i].y]=0;
            world[tabpredateur[i].x][tabpredateur[i].y-1]=2;
        }
        if(M[i]==2 && bon_voisin(world,tabpredateur[i].x+1,tabpredateur[i].y))
        {
            world[tabpredateur[i].x][tabpredateur[i].y]=0;
            world[tabpredateur[i].x+1][tabpredateur[i].y]=2;
        }
        if(M[i]==3 && bon_voisin(world,tabpredateur[i].x,tabpredateur[i].y+1))
        {
            world[tabpredateur[i].x][tabpredateur[i].y]=0;
            world[tabpredateur[i].x][tabpredateur[i].y+1]=2;
        }
    } 
}

int proie_kill (int **world)
{
    int nb_predateur=0;
    int nb_kill = 0;

    for (int i=0;i<T;i++)
    {
        for (int j=0;j<T;j++)
        {
            if (world[i][j]==1)
            {
                if (i-1>=0)
                {
                    if(world[i-1][j]==2)
                    {
                        nb_predateur+=1;
                    }
                    if (j-1>=0)
                    {
                        if(world[i-1][j-1]==2)
                        {
                            nb_predateur+=1;
                        }
                    }
                    if (j+1<T)
                    {
                        if(world[i-1][j+1]==2)
                        {
                            nb_predateur+=1;
                        }
                    }
                }
                if (i+1<T)
                {
                    if(world[i+1][j]==2)
                    {
                        nb_predateur+=1;
                    }
                    if (j-1>=0)
                    {
                        if(world[i+1][j-1]==2)
                        {
                            nb_predateur+=1;
                        }
                    }
                    if (j+1<T)
                    {
                        if(world[i+1][j+1]==2)
                        {
                            nb_predateur+=1;
                        }
                    }
                }
                if (j-1>=0)
                {
                    if(world[i][j-1]==2)
                    {
                        nb_predateur+=1;
                    }
                }
                if (j+1<T)
                {
                    if(world[i][j+1]==2)
                    {
                        nb_predateur+=1;
                    }
                }

                if (nb_predateur>1)
                {
                    world[i][j]=3;
                    int e1=0;
                    while(e1==0)
                    {
                        int kx = rand()%T;
                        int ky = rand()%T;
                        if (world[kx][ky]==0 && e1!=1 )
                        {
                            world[kx][ky]=1;
                            e1=1;
                        }
                    }
                    nb_kill+=1;
                }
                nb_predateur=0;
            }
        }
    }
    return nb_kill;
}

void afficher_kill(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window)
{
    SDL_Rect proie= {0,0,207,197};
    SDL_Rect destination = {0,0,0,0};
    SDL_Rect window_dimensions= {0,0,0,0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    for (int i =0; i<T;i++)
    {
        for (int j = 0; j<T ; j++)
        {
            if (world[j][i]==3)
            {
                destination.x=i*window_dimensions.w/T;
                destination.y=j*window_dimensions.h/T;
                destination.w=window_dimensions.w/T;
                destination.h=window_dimensions.h/T;
                SDL_RenderCopy(renderer,sprite,&proie,&destination);
            }
        }
    }
}

void clean_world (int **world)
{
    for (int i=0;i<T;i++)
    {
        for (int j=0;j<T;j++)
        {
            if (world[i][j]==3)
            {
                world[i][j]=0;
            }
        }
    }
}

int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup,int nb_proie)
{
    int indice=0;
    int min_dist= sqrt((loup.x-proie[0].x)*(loup.x-proie[0].x)+(loup.y-proie[0].y)*(loup.y-proie[0].y));
    for(int k=1; k<nb_proie;k++)
    {
        int distance=sqrt((loup.x-proie[k].x)*(loup.x-proie[k].x)+(loup.y-proie[k].y)*(loup.y-proie[k].y));
        if(distance<min_dist)
        {
            min_dist=distance;
            indice=k;
        }
    }
    return indice;
}

int position( emplacement_t loup, emplacement_t proie)
{
    int resultat;
    float i=(float)rand() / (float)RAND_MAX;
    if (loup.x>proie.x)
    {
        if(loup.y>proie.y)
        {
            if(i<0.5)
            {
                resultat=1;
            }
            else
                resultat=0;             
        }
        else if(loup.y<proie.y)
        {
            if(i<=0.5)
            {
                resultat=3;
            }
            else
            {
                resultat=0;
            } 
        }
        else
        {
            resultat=0;
        }
    }  
    else if(loup.x<proie.x)
    {
        if(loup.y>proie.y)
        {
            if(i<=0.5)
            {
                resultat=2;
            }
            else
                resultat=1;
                
        }
        else if(loup.y<proie.y)
        {
            if(i<=0.5)
            {
                resultat=3;
            }
            else
            {
                resultat=2;
            }
        }
        else
        {
            resultat=2;
        }
    }
    else
    {
        if(loup.y>proie.y)
        {
            resultat=1;     
        }
        else if(loup.y<proie.y)
        {
            resultat=3;
        }
        else
            resultat = 0; // won't happen
    }
    return resultat; 
}

int distance_proie(emplacement_t loup, emplacement_t proie)
{
    int resultat;
    int distance = sqrt((loup.x-proie.x)*(loup.x-proie.x)-(loup.y-proie.y)*(loup.y-proie.y));
    if(distance<7)
    {
        resultat=0;
    }
    else
        resultat=1;

    return resultat;
}

int loup_le_plus_proche(emplacement_t loup[], int k)
{
    int resultat;
    if (k==0)
    {
        resultat = position(loup[0], loup[1]);
    }
    else
        resultat = position(loup[1], loup[0]);

    return resultat;
}

int distance_loup(emplacement_t tabloup[])
{
    int resultat;
    resultat=distance_proie(tabloup[0], tabloup[1]);
    return resultat;
}

void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[],int nb_proie) //
{
    for (int k=0;k<NB_LOUP;k++)
    {
        int indice=proie_la_plus_proche(tabproie, tabloup[k],nb_proie);  //recherche de la proie la plus 
        int direction = position(tabloup[k], tabproie[indice]); //position proie N,S,E,O
        etat[2*k].pos = direction;
        int longueur = distance_proie(tabloup[k], tabproie[indice]); // distance de la proie P, L
        etat[2*k].dis=longueur;

        int loup=loup_le_plus_proche(tabloup, 0); // position loup N,S,E,O
        etat[2*k+1].pos=loup;
        longueur = distance_loup(tabloup); // distance loup P ou L
        etat[2*k+1].dis=longueur;
    }
}

double somme_calcul_proba_event(TabRegle tabregle, int s, int regle_accepte[]) //calcul de la somme des priorités en fonction des règles sélectionées
{
    double x=0;
    for (int i = 0; i < nb_regle ; i++)
    {
        if (regle_accepte[i]==1)
        {
            x = x + pow((tabregle).tab[i].priorite + 1, s);
        }
    }
    return x;
}

void calcul_proba_event(TabRegle tabregle, float s, double x, double L[], int regle_accepte[]) //calcul de chaque probabilité pour chaque règle sélectionée en fonction de leur priorité
{
    float S=0;
    float tmp;
    for (int i = 0; i < nb_regle ; i++)
    {
        if(regle_accepte[i]==1)
        {
            tmp= pow((tabregle).tab[i].priorite + 1, s)/x; 
            S = S + tmp;
            L[i]=S;
        }
        else
        {
            L[i]=0;
        }
    }
}

int choix_proba(double L[], float nombre) //tirage aléatoire de la règle qui sera choisie en fonction des probabilités calculées
{
    int resultat=0;
    while(L[resultat]<nombre)
    {
            resultat=resultat +1;
    }
    return resultat;
}

void choix_regles(observation_t ob_proie,observation_t ob_loup, int regle_accepte[], TabRegle tabregle) //fonction qui donne les règles en accord avec la perception du loup
{
    for (int k=0; k<nb_regle; k++)
    {
        if((ob_proie.pos==(tabregle).tab[k].proie.pos) ||((tabregle).tab[k].proie.pos==-1))
        {
            if((ob_proie.dis==(tabregle).tab[k].proie.dis) || ((tabregle).tab[k].proie.dis==-1))
            {
                if((ob_loup.pos==(tabregle).tab[k].predateur.pos)||((tabregle).tab[k].predateur.pos==-1))
                {
                    if((ob_loup.dis==(tabregle).tab[k].predateur.dis)||((tabregle).tab[k].predateur.dis==-1))
                    {
                        regle_accepte[k]=1;
                    }
                }
            }
        }
    }
}

void move_predateur(emplacement_t tabloup[],emplacement_t tabproie[], int M[],TabRegle tabregle, int nb_proie)
{
    float nombre = 0;

    nombre = (float)rand() / (float)RAND_MAX;

    int regle_accepte[nb_regle];

    observation_t etat[2*NB_LOUP];

    etat_loup(tabloup, tabproie, etat, nb_proie);

    for (int i=0; i<NB_LOUP; i++)
    {
        double x1=0;

        for(int k=0;k<nb_regle; k++)
        {
            regle_accepte[k]=0;
        }
        choix_regles(etat[2*i], etat[2*i+1], regle_accepte, tabregle);

        x1 = somme_calcul_proba_event(tabregle, 1, regle_accepte);

        double L[nb_regle];
        calcul_proba_event(tabregle, 1, x1, L, regle_accepte);

        int indice = choix_proba(L, nombre);
        
        M[i]=(tabregle).tab[indice].action;
    }
}

void shuffle(int *array, int size)
{   
    for (int i = size - 1; i > 0; i--)
    {
        int j = rand() % (i + 1);
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

int rechercher_max_distance_position(int L[], int k)
{
    int max=L[0];
    int indice=-1;
    for(int i=1; i<k; i++)
    {
        if(L[i]>max)
        {
            max=L[i];
            indice=i-1;
        }
    }
    return indice;
}

int rechercher_max_priorite(int L[], int k)
{
    int max=L[0];
    int indice=1;
    for(int i=1; i<k; i++)
    {
        if(L[i]>max)
        {
            max=L[i];
            indice=i+1;
        }
    }
    return indice;
}

int rechercher_max_score(int L[], int k)
{
    int max=L[0];
    int indice=0;
    for(int i=1; i<k; i++)
    {
        if(L[i]>max)
        {
            max=L[i];
            indice=i;
        }
    }
    return indice;
}

float moyenne_thread(TabRegle tabregle)
{
    int res[5];
    int sum=0;
    thrd_t thread_a;
    thrd_t thread_b;
    thrd_t thread_c;
    thrd_t thread_d;
    thrd_t thread_e;
    

    thrd_create(&thread_a,moyenne_IA,&tabregle);
    thrd_create(&thread_b,moyenne_IA,&tabregle);
    thrd_create(&thread_c,moyenne_IA,&tabregle);
    thrd_create(&thread_d,moyenne_IA,&tabregle);
    thrd_create(&thread_e,moyenne_IA,&tabregle);

    thrd_join(thread_a,&res[0]);
    thrd_join(thread_b,&res[1]);
    thrd_join(thread_c,&res[2]);
    thrd_join(thread_d,&res[3]);
    thrd_join(thread_e,&res[4]);

    for (int i=0;i<5;i++)
    {
        sum=sum+res[i];
    }

    float moy = (float)sum/5;
    printf("res: %f\n", moy);

    return moy;
}

TabRegle changement_regle(FILE* flux)
{
    TabRegle tabregle;
    tabregle = chargerTabRegle(flux);

    int *tabindice = malloc(sizeof(int) *6*nb_regle);

    for (int i =0 ; i< 6*nb_regle ; i++)
    {
        tabindice[i] = i;
    }

    shuffle(tabindice, 6*nb_regle);
    int score=0;
    int attribut;

    for (int i = 0; i < 6*nb_regle; i++)
    {
        int indice = tabindice[i];
        int ligne = indice/6;
        int colonne = indice%6;
        int limit_inf, limit_sup;
        
        
        if (colonne == 0)
        {
            int liste[5];
            limit_inf = -1, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].proie.pos = k;
                liste[k+1] = moyenne_thread(tabregle);
            }
            attribut=rechercher_max_distance_position(liste, 5);
            tabregle.tab[ligne].proie.pos = attribut;
        }
        if (colonne == 2)
        {
            int liste[5];
            limit_inf = -1, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].predateur.pos = k;
                score = moyenne_thread(tabregle);
                liste[k+1] = score;
            }
            attribut=rechercher_max_distance_position(liste, 5);
            tabregle.tab[ligne].predateur.pos = attribut;
        }
        if (colonne == 1)
        {
            int liste[3];
            limit_inf = -1, limit_sup = 1;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].proie.dis = k;
                score = moyenne_thread(tabregle);
                liste[k+1] = score;
            }
            attribut=rechercher_max_distance_position(liste, 3);
            tabregle.tab[ligne].proie.dis = attribut;

        }
        if (colonne == 3)
        {
            int liste[3];
            limit_inf = -1, limit_sup = 1;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].predateur.dis = k;
                score = moyenne_thread(tabregle);
                liste[k+1] = score;
            }
            attribut=rechercher_max_distance_position(liste, 3);
            tabregle.tab[ligne].predateur.dis = attribut;
        }
        if (colonne == 4)
        {
            int liste[4];
            limit_inf = 0, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].action = k;
                score = moyenne_thread(tabregle);
                liste[k] = score;
            }
            attribut=rechercher_max_score(liste, 4);
            tabregle.tab[ligne].action = attribut;

        }
        if (colonne == 5)
        {
            int liste[5];
            limit_inf = 1, limit_sup = 5;
            for ( int k = limit_inf; k <= limit_sup; k++)
            {
                tabregle.tab[ligne].priorite = k;
                score = moyenne_thread(tabregle);
                liste[k-1] = score;
            }
            attribut=rechercher_max_priorite(liste, 5);
            tabregle.tab[ligne].priorite = attribut;

        }
    }
    return tabregle;
}

int moyenne_IA(void* tabregle_void)
{
    int sum=0;
    for (int i=0;i<20;i++)
    {
        sum+=IA(tabregle_void);
    }
    return sum/20;
}

int IA(void *tabregle_void)
{
    TabRegle tabregle= *((TabRegle*)tabregle_void);
    int **world = creat_world();
    int nb_proie = 4;
    int nb_kill = 0;
    loc_proie(world);
    loc_predateur(world);
    
    int e=0;
    int score=0;

    while (e<100)
    {
        deplacer_proie(world,nb_proie);
        deplacer_predateur(world,tabregle,nb_proie);
        nb_kill+=proie_kill(world);  
        clean_world(world);
        e+=1;
    }

    score= round(100000*(float)(nb_kill)/100);

    free_world(world);
    return score;
}