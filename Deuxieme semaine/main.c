#include "jeu.h"
#include <time.h>
#include <math.h>

int IA4(TabRegle tabregle)
{
    int **world = creat_world();
    int nb_proie = 4;
    int nb_kill = 0;
    loc_proie(world);
    loc_predateur(world);
    SDL_Window *window; 
    SDL_Renderer *renderer;
    SDL_bool program_on = SDL_TRUE;
    int e=0;
    int score=0;
    SDL_Event event; 
    SDL_Texture *my_texture;
    SDL_Texture *sprite_proie;
    SDL_Texture *sprite_predateur;
    SDL_Texture *sprite_kill;

    afficherTabRegle(stdout, tabregle);


    window = SDL_CreateWindow("jeu",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,1000,1000,SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",SDL_GetError()); 
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (renderer == NULL)
    {
        SDL_Log("Error : Renderer - %s\n", SDL_GetError()); 
    }

    sprite_proie = IMG_LoadTexture(renderer, "proie1.png");
    if (sprite_proie == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }
    
    sprite_predateur = IMG_LoadTexture(renderer, "predateur1.png");
    if (sprite_predateur == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }

    sprite_kill = IMG_LoadTexture(renderer, "coup1.png");
    if (sprite_kill == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }

    my_texture = IMG_LoadTexture(renderer, "background.jpg");

    if (my_texture == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }

    SDL_RenderClear(renderer);
    background(my_texture,window,renderer);

    while (program_on == SDL_TRUE && e<100)
    {
        while (SDL_PollEvent(&event))
        {                                                   // Tant que on n'a pas trouvé d'évènement utile                                    // et la file des évènements stockés n'est pas vide et qu'on n'a pas                                             // terminé le programme Défiler l'élément en tête de file dans 'event'
            switch (event.type)
            {                                               // En fonction de la valeur du type de cet évènement
                case SDL_QUIT:                              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                    program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
                    break;

                case SDL_KEYDOWN:                           // Le type de event est : une touche appuyée
                                                            // comme la valeur du type est SDL_Keydown, dans la partie 'union' de
                                                            // l'event, plusieurs champs deviennent pertinents   
                    switch (event.key.keysym.sym)
                    {           
          
                        case SDLK_SPACE:                            
                            program_on = SDL_FALSE;                           // 'escape' ou 'q', d'autres façons de quitter le programme                     
                            break;
                    }
                default:
                    break;
            }
            SDL_RenderClear(renderer);
            background(my_texture,window,renderer);
        }
        for (int i=0; i<8;i++)
        {
            if (i==0)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur1.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie1.png");
            }
            if (i==1)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur2.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie2.png");
            }
            if (i==2)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur3.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie3.png");
            }
            if (i==3)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur4.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie4.png");
            }
            if (i==4)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur5.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie5.png");
            }
            if (i==5)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur6.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie6.png");
            }
            if (i==6)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur7.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie7.png");
            }
            if (i==7)
            {
                sprite_predateur=IMG_LoadTexture(renderer, "predateur7.png");
                sprite_proie = IMG_LoadTexture(renderer, "proie8.png");
            }
            SDL_RenderClear(renderer);
            background(my_texture,window,renderer);
            afficher_proie(sprite_proie,renderer,world,window);
            afficher_predateur(sprite_predateur,renderer,world,window);
            SDL_RenderPresent(renderer);
            SDL_Delay(10);
        }
        SDL_Delay(10);
        int precedent = nb_kill;
        deplacer_proie(world,nb_proie);
        deplacer_predateur(world,tabregle,nb_proie);
        nb_kill+=proie_kill(world);
        if (precedent!=nb_kill)
        {
            for (int i=0;i<21;i++)
            {
                if (i==0)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup1.png");
                }
                if (i==1)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup2.png");
                }
                if (i==2)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup3.png");
                }
                if (i==3)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup4.png");
                }
                if (i==4)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup5.png");
                }
                if (i==5)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup6.png");
                }
                if (i==6)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup7.png");
                }
                if (i==7)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup8.png");
                }
                if (i==8)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup9.png");
                }
                if (i==9)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup10.png");
                }
                if (i==10)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup11.png");
                }
                if (i==11)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup12.png");
                }
                if (i==12)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup13.png");
                }
                if (i==13)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup14.png");
                }
                if (i==14)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup15.png");
                }
                if (i==15)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup16.png");
                }
                if (i==16)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup17.png");
                }
                if (i==17)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup18.png");
                }
                if (i==18)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup19.png");
                }
                if (i==19)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup20.png");
                }
                if (i==20)
                {
                    sprite_kill = IMG_LoadTexture(renderer, "coup21.png");
                }
                SDL_RenderClear(renderer);
                background(my_texture,window,renderer);
                afficher_proie(sprite_proie,renderer,world,window);
                afficher_predateur(sprite_predateur,renderer,world,window);
                afficher_kill(sprite_kill,renderer,world,window);
                SDL_DestroyTexture(sprite_kill);
                SDL_RenderPresent(renderer);
                SDL_Delay(10);
            }
            for (int i =0; i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    if (j==0)
                    {
                        sprite_predateur=IMG_LoadTexture(renderer, "predateur_kill1.png");
                    }
                    if (j==1)
                    {
                        sprite_predateur=IMG_LoadTexture(renderer, "predateur_kill2.png");
                    }
                    if (j==2)
                    {
                        sprite_predateur=IMG_LoadTexture(renderer, "predateur_kill3.png");
                    }
                    SDL_RenderClear(renderer);
                    background(my_texture,window,renderer);
                    afficher_proie(sprite_proie,renderer,world,window);
                    afficher_predateur(sprite_predateur,renderer,world,window);
                    SDL_DestroyTexture(sprite_predateur);
                    SDL_RenderPresent(renderer);
                    SDL_Delay(10);
                }

            }
        }
        clean_world(world);
        e+=1;
    }
    
    score= round(100000*(float)(nb_kill)/100);
    
    SDL_RenderClear(renderer);
    background(my_texture,window,renderer);
    SDL_RenderPresent(renderer);
    SDL_Delay(1000);
    
    SDL_DestroyTexture(my_texture);
    SDL_DestroyTexture(sprite_proie);
    SDL_DestroyTexture(sprite_predateur);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    IMG_Quit();
    SDL_Quit();
    free_world(world);
    return score;
}



int main()
{
    srand(time(NULL));
    FILE* flux;

    TabRegle tabregle;
/*
    for (int i =0; i<10;i++)
    {
        flux=fopen("regle1.txt", "r");
        tabregle = changement_regle(flux);
        fclose(flux);
        flux = fopen("regle1.txt", "w");
        afficherTabRegle(flux,tabregle);
        fclose(flux);
    }
/*/
    flux=fopen("regle1.txt", "r");
    tabregle = chargerTabRegle(flux);
    fclose(flux);
//*/

    int score = IA4(tabregle);

    printf("score final  = %d\n",score);
    freeTabregle(tabregle);
    return 0; 
}

// -fsanitize=address 
// int main()
// {
//     srand(time(NULL));

//     changement_regle(flux);

//     for(int k=0; k<4;k=k+2)
//     {
//         double x=0;
//         for(int k=0;k< NB_REGLE; k++)
//         {
//             regle_accepte[k]=0;
//         }

//         choix_regles(etat[k], etat[k+1], regle_accepte, tabregle);

//         printf(" les règle choisies sont : ");
//         for(int k=0;k< NB_REGLE; k++)
//         {
//             printf("%d", regle_accepte[k]);
//         }
//         printf("\n");

//         x = somme_calcul_proba_event(tabregle, 1, regle_accepte);

//         printf("%f\n", x);

//         double L[NB_REGLE];
//         calcul_proba_event(tabregle, 1, x, L, regle_accepte);
//         for(int k=0;k<NB_REGLE; k++)
//         {
//             printf("%f ", L[k]);
//         }
        
//         printf("\n");

//         int indice = choix_proba(L, nombre);

//         printf("%f %d\n", nombre, indice);

//         printf("\n");

//         printf("%d %d %d %d -> %d (%d)", tabregle.tab[indice].proie.pos,
//                 tabregle.tab[indice].proie.dis,
//                 tabregle.tab[indice].predateur.pos,
//                 tabregle.tab[indice].predateur.dis,
//                 tabregle.tab[indice].action,
//                 tabregle.tab[indice].priorite);
//         printf("\n");
//     }
//     return 0;
// }
