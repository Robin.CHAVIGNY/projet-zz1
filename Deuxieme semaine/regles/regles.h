#include <stdio.h>
#include<string.h>

#define nb_regle 10


typedef enum {
    JOKER1 =-1,N, O, S, E
} position_t;

typedef enum {
    JOKER2 =-1, P, L
} distance_t;

typedef struct {
    position_t pos;
    distance_t dis;
} observation_t;


typedef enum {
    NN, OO, SS, EE
} action_t;


typedef struct {
    observation_t proie;
    observation_t predateur;
    action_t action;
    int priorite;
} Regle;

typedef struct{
    Regle tab[nb_regle];
}TabRegle;

void afficherRegle(FILE* flux, Regle regle);

Regle chargerRegle(FILE* flux);
TabRegle chargerTabRegle(FILE* flux);
void afficherTabRegle(FILE* flux, const TabRegle tabRegle);






