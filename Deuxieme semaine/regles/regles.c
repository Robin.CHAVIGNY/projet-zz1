#include <stdio.h>
#include <string.h>
#include "regles.h"


Regle chargerRegle(FILE* flux)
{
    Regle regle;
    fscanf(flux, " - [%d, %d, %d, %d] --> %d (%d)",
                    &regle.proie.pos,
                    &regle.proie.dis,
                    &regle.predateur.pos,
                    &regle.predateur.dis,
                    &regle.action,
                    &regle.priorite);
    return regle;
}

TabRegle chargerTabRegle(FILE* flux)
{
    TabRegle tabRegle;
    for(int k=0; k<nb_regle; k++)
    {
        Regle regle;
        regle=chargerRegle(flux);
        tabRegle.tab[k]=regle;
    }
    return tabRegle;
}

void afficherRegle(FILE* flux, Regle regle)
{
    fprintf(flux, "- [%d, %d, %d, %d] --> %d (%d)\n",
            regle.proie.pos, regle.proie.dis,
            regle.predateur.pos, regle.predateur.dis,
            regle.action, regle.priorite);
}

void afficherTabRegle(FILE* flux, TabRegle tabRegle)
{
    for(int k=0; k<nb_regle; k++)
    {
        afficherRegle(flux, tabRegle.tab[k]);
    }
}


int main()
{
    FILE* flux=fopen("./regle.txt", "r");
    TabRegle tabRegle=chargerTabRegle(flux);
    afficherTabRegle(stdout, tabRegle);
    fclose(flux);
}
