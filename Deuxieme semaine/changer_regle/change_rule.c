#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <time.h>
#include "change_rule.h"


int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup) //fonction qui renvoie l'indice de la proie la plus proche pour un loup
{
    int indice=0;
    int min_dist= sqrt((loup.x-proie[0].x)*(loup.x-proie[0].x)+(loup.y-proie[0].y)*(loup.y-proie[0].y));
    for(int k=1; k<3;k++)
    {
        int distance=sqrt((loup.x-proie[k].x)*(loup.x-proie[k].x)+(loup.y-proie[k].y)*(loup.y-proie[k].y));
        if(distance<min_dist)
        {
            min_dist=distance;
            indice=k;
        }
    }
    return indice;
}

int position(emplacement_t loup, emplacement_t proie) //fonction qui renvoie l'enum (0, 1, 2 ou 3) de la position de la proie par rapport au loup
{
    int resultat;
    float i = (float)rand() / (float)RAND_MAX;
    if (loup.x > proie.x)
    {
        if(loup.y > proie.y)
        {
            if(i < 0.5)
            {
                resultat = 1; //Ouest
            }
            else
                resultat = 0; //Nord           
        }
        if(loup.y <= proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 2; //Sud
            }
            else
            {
                resultat = 1; //Ouest
            } 
        }

    }  
    else
    {
        if(loup.y > proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 0; //Nord
            }
            else
                resultat = 3; //Est
                    
        }
        if(loup.y <= proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 3; //Est
            }
            else
            {
                resultat = 2; //Sud
            }            
        }
    }
    return resultat; 
}

int distance_proie(emplacement_t loup, emplacement_t proie) //fonction qui renvoie l'enum (0 ou 1) de la distance de la proie par rapport au loup
{
    int resultat;
    int distance = sqrt((loup.x-proie.x)*(loup.x-proie.x)-(loup.y-proie.y)*(loup.y-proie.y));
    if(distance < 7)
    {
        resultat = 0; //proche
    }
    else
        resultat = 1; //loin

    return resultat;
}

int loup_le_plus_proche(emplacement_t loup[], int k) //renvoie l'enum de la position du loup le plus proche
{
    int resultat;
    if (k == 0) //proche
    {
        resultat = position(loup[0], loup[1]); 
    }
    else //loin
        resultat = position(loup[1], loup[0]); 

    return resultat;
}

int distance_loup(emplacement_t tabloup[]) //renvoie l'enum de la distance entre les deux loups
{
    int resultat;
    resultat = distance_proie(tabloup[0], tabloup[1]);
    return resultat;
}

void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[]) //fonction qui donne l'etat global d'un loup (sa perception)
{
    for(int k = 0; k < 4; k = k+2)
    {
        int indice = proie_la_plus_proche(tabproie, tabloup[k]);  //recherche de la proie la plus 
        int direction = position(tabloup[k], tabproie[indice]); //position proie N,S,E,O
        etat[k].pos = direction;
        int longueur = distance_proie(tabloup[k], tabproie[indice]); // distance de la proie P, L
        etat[k].dis = longueur;
        int loup = loup_le_plus_proche(tabloup, k); // position loup N,S,E,O
        etat[k+1].pos = loup;
        longueur = distance_loup(tabloup); // distance loup P ou L
        etat[k+1].dis = longueur;
    }
}

double somme_calcul_proba_event(TabRegle tableau, int s, int regle_accepte[]) //fonction qui renvoie la somme des priorités des règles sélectionnées
{
    double x=0;
    for (int i = 0; i < NB_REGLE ; i++)
    {
        if (regle_accepte[i] == 1)
        {
            x = x + pow(tableau.tab[i].priorite + 1, s);
        }
    }
    return x;
}

void calcul_proba_event(TabRegle tableau, float s, double x, double L[], int regle_accepte[]) //fonction qui calcule la probabilité d'un règle d'être choisie et donne les probabilités cumulées
{
    float S = 0;
    float tmp;
    for (int i = 0; i < NB_REGLE ; i++)
    {
        if(regle_accepte[i] == 1)
        {
            tmp = pow(tableau.tab[i].priorite + 1, s)/x; 
            S = S + tmp;
            L[i] = S;
        }
    }
}

int choix_proba(double L[], float nombre) //fonction qui renvoie l'indice de la règle choisie avec un tirage d'un nombre aléatoire
{
    int k = 0;
    while (nombre>L[k])
    {
        k++;
    }
    return k;
}

Regle chargerRegle(FILE* flux) //fonction qui charge une règle depuis un flux
{
    Regle regle;
    fscanf(flux, " - [%d, %d, %d, %d] --> %d (%d)",
                    &regle.proie.pos,
                    &regle.proie.dis,
                    &regle.predateur.pos,
                    &regle.predateur.dis,
                    &regle.action,
                    &regle.priorite);
    return regle;
}

TabRegle chargerTabRegle(FILE* flux) //fonction qui charge un tableau de règles depuis un flux
{
    TabRegle tabRegle;
    tabRegle.tab = malloc(NB_REGLE*sizeof(Regle));
    for(int k = 0; k < NB_REGLE; k++)
    {
        Regle regle;
        regle = chargerRegle(flux);
        tabRegle.tab[k] = regle;
    }
    return tabRegle;
}

void afficherRegle(FILE* flux, Regle regle) //fonction qui affiche une règle
{
    fprintf(flux, "- [%d, %d, %d, %d] --> %d (%d)\n",
            regle.proie.pos, regle.proie.dis,
            regle.predateur.pos, regle.predateur.dis,
            regle.action, regle.priorite);
}

void afficherTabRegle(FILE* flux, TabRegle tabRegle) //fonction qui affiche un tableau de règles
{
    for(int k = 0; k < NB_REGLE; k++)
    {
        afficherRegle(flux, tabRegle.tab[k]);
    }
}

void freeTabregle(TabRegle tabregle) //fonction qui libère le tableau de règles
{
    free(tabregle.tab);
}

void choix_regles(observation_t loup1, observation_t loup2, int regle_accepte[], TabRegle tabregle) //fonction qui donne les règles sélectionnées en fonction de la perception des loups
{
    for (int k=0; k< NB_REGLE; k++)
    {
    if((loup1.pos==tabregle.tab[k].proie.pos)||(tabregle.tab[k].proie.pos==-1))
    {
        if((tabregle.tab[k].proie.dis==-1)||(loup1.dis==tabregle.tab[k].proie.dis))
        {
            if((loup2.pos==tabregle.tab[k].predateur.pos)||(tabregle.tab[k].predateur.pos==-1))
            {
                if((loup2.dis==tabregle.tab[k].predateur.dis)||(tabregle.tab[k].predateur.dis==-1))
                {
                    regle_accepte[k]=1;
                }
            }
        }
    }
    }
}

void shuffle(int *array, int size) //fonction qui mélange une liste de manière aléatoire
{
    srand(time(NULL));
    
    for (int i = size - 1; i > 0; i--)
    {
        int j = rand() % (i + 1);
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

int rechercher(int L[], int k) //fonction qui renvoie le maximum d'une liste, ici dans le cas de la position ou de la distance (-1 à 3 ou -1 à 1)
{
    int max = L[0];
    int indice = -1;
    for(int i = 1; i < k; i++)
    {
        if(L[i] > max)
        {
            max = L[i];
            indice = i-1;
        }
    }
    return indice;
}

int rechercher2(int L[], int k) //fonction qui renvoie le maximum d'une liste, ici dans le cas de la priorité (1 à 5)
{
    int max = L[0];
    int indice = 1;
    for(int i = 1; i < k; i++)
    {
        if(L[i] > max)
        {
            max = L[i];
            indice = i+1;
        }
    }
    return indice;
}

int rechercher3(int L[], int k) //fonction qui renvoie le maximum d'une liste, ici dans le cas de l'action (0 à 3)
{
    int max = L[0];
    int indice = 0;
    for(int i = 1; i < k; i++)
    {
        if(L[i] > max)
        {
            max = L[i];
            indice = i;
        }
    }
    return indice;
}

void changement_regle(FILE* flux) //fonction qui change un attribut d'une règle en fonction de l'indice de la liste
{
    TabRegle tabregle;
    tabregle = chargerTabRegle(flux);
    afficherTabRegle(stdout,tabregle);

    int *tabindice = malloc(sizeof(int) *60);

    for (int i = 0 ; i < 60 ; i++)
    {
        tabindice[i] = i;
    }

    shuffle(tabindice, 60);
    int score = 0;
    int attribut;

    for (int i = 0; i < 60; i++)
    {
        int indice = tabindice[i];
        printf("%d\n", indice);

        int ligne = (int)indice/6;
        printf("%d\n", ligne);

        int colonne = indice%6;
        printf("%d\n", colonne);

        int limit_inf, limit_sup;
        
        
        if (colonne == 0)
        {
            int liste[5];
            limit_inf = -1, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].proie.pos = k;
                liste[k] = score;
            }
            attribut=rechercher(liste, 5);
            tabregle.tab[ligne].priorite = attribut;
        }
        if (colonne == 2)
        {
            int liste[5];
            limit_inf = -1, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].predateur.pos = k;
                liste[k] = score;
            }
            attribut=rechercher(liste, 5);
            tabregle.tab[ligne].priorite = attribut;
        }
        if (colonne == 1)
        {
            int liste[3];
            limit_inf = -1, limit_sup = 1;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].proie.dis = k;
                liste[k] = score;
            }
            attribut=rechercher(liste, 3);
            tabregle.tab[ligne].priorite = attribut;

        }
        if (colonne == 3)
        {
            int liste[3];
            limit_inf = -1, limit_sup = 1;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].predateur.dis = k;
                liste[k] = score;
            }
            attribut=rechercher(liste, 3);
            tabregle.tab[ligne].priorite = attribut;
        }
        if (colonne == 4)
        {
            int liste[4];
            limit_inf = 0, limit_sup = 3;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].action = k;
                liste[k] = score;
            }
            attribut=rechercher3(liste, 4);
            tabregle.tab[ligne].priorite = attribut;

        }
        if (colonne == 5)
        {
            int liste[5];
            limit_inf = 1, limit_sup = 5;
            for ( int k = limit_inf; k <= limit_sup; k++)
            { 
                tabregle.tab[ligne].priorite = k;
                liste[k] = score;
            }
            attribut=rechercher2(liste, 5);
            tabregle.tab[ligne].priorite = attribut;

        }
        printf("%d\n", attribut);
        printf("\n");  
    }
}

int main()
{
    TabRegle tabregle;
    FILE* flux = fopen("./regles2.txt", "r");
    tabregle = chargerTabRegle(flux);
    afficherTabRegle(stdout, tabregle);
    printf("\n");

    float nombre = 0;

    srand(time(NULL));
    nombre = (float)rand() / (float)RAND_MAX;

    int regle_accepte[NB_REGLE];

    emplacement_t tabloup[2];
    emplacement_t tabproie[3];
    observation_t etat[4];

    changement_regle(flux);

    for(int k = 0; k < 4;k = k+2)
    {
        double x = 0;

        for(int k = 0; k < NB_REGLE; k++)
        {
            regle_accepte[k] = 0;
        }

        choix_regles(etat[k], etat[k+1], regle_accepte, tabregle);

        printf(" les règle choisies sont : ");
        for(int k = 0; k < NB_REGLE; k++)
        {
            printf("%d", regle_accepte[k]);
        } 

    }
        
    printf("\n");

    int indice = choix_proba(L, nombre);

    printf("%f %d\n", nombre, indice);

    printf("\n");

    printf("%d %d %d %d -> %d (%d)", tabregle.tab[indice].proie.pos,
                tabregle.tab[indice].proie.dis,
                tabregle.tab[indice].predateur.pos,
                tabregle.tab[indice].predateur.dis,
                tabregle.tab[indice].action,
                tabregle.tab[indice].priorite);
    printf("\n");


    fclose(flux);
    return 0;
}
