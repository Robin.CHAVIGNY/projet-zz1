#include <stdio.h>
#include <math.h>
#include "mvt_loup.h"
#include <time.h>
#include <stdlib.h>

int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup) //fonction qui renvoie l'indice de la proie la plus proche pour un loup
{
    int indice=0;
    int min_dist= sqrt((loup.x-proie[0].x)*(loup.x-proie[0].x)+(loup.y-proie[0].y)*(loup.y-proie[0].y));
    for(int k=1; k<3;k++)
    {
        int distance=sqrt((loup.x-proie[k].x)*(loup.x-proie[k].x)+(loup.y-proie[k].y)*(loup.y-proie[k].y));
        if(distance<min_dist)
        {
            min_dist=distance;
            indice=k;
        }
    }
    return indice;
}

int position(emplacement_t loup, emplacement_t proie) //fonction qui renvoie l'enum (0, 1, 2 ou 3) de la position de la proie par rapport au loup
{
    int resultat;
    float i = (float)rand() / (float)RAND_MAX;
    if (loup.x > proie.x)
    {
        if(loup.y > proie.y)
        {
            if(i < 0.5)
            {
                resultat = 1; //Ouest
            }
            else
                resultat = 0; //Nord           
        }
        if(loup.y <= proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 2; //Sud
            }
            else
            {
                resultat = 1; //Ouest
            } 
        }

    }  
    else
    {
        if(loup.y > proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 0; //Nord
            }
            else
                resultat = 3; //Est
                    
        }
        if(loup.y <= proie.y)
        {
            if(i <= 0.5)
            {
                resultat = 3; //Est
            }
            else
            {
                resultat = 2; //Sud
            }            
        }
    }
    return resultat; 
}

int distance_proie(emplacement_t loup, emplacement_t proie) //fonction qui renvoie l'enum (0 ou 1) de la distance de la proie par rapport au loup
{
    int resultat;
    int distance = sqrt((loup.x-proie.x)*(loup.x-proie.x)-(loup.y-proie.y)*(loup.y-proie.y));
    if(distance < 7)
    {
        resultat = 0; //proche
    }
    else
        resultat = 1; //loin

    return resultat;
}

int loup_le_plus_proche(emplacement_t loup[], int k) //renvoie l'enum de la position du loup le plus proche
{
    int resultat;
    if (k == 0) //proche
    {
        resultat = position(loup[0], loup[1]); 
    }
    else //loin
        resultat = position(loup[1], loup[0]); 

    return resultat;
}

int distance_loup(emplacement_t tabloup[]) //renvoie l'enum de la distance entre les deux loups
{
    int resultat;
    resultat = distance_proie(tabloup[0], tabloup[1]);
    return resultat;
}

void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[]) //fonction qui donne l'etat global d'un loup (sa perception)
{
    for(int k = 0; k < 4; k = k+2)
    {
        int indice = proie_la_plus_proche(tabproie, tabloup[k]);  //recherche de la proie la plus 
        int direction = position(tabloup[k], tabproie[indice]); //position proie N,S,E,O
        etat[k].pos = direction;
        int longueur = distance_proie(tabloup[k], tabproie[indice]); // distance de la proie P, L
        etat[k].dis = longueur;
        int loup = loup_le_plus_proche(tabloup, k); // position loup N,S,E,O
        etat[k+1].pos = loup;
        longueur = distance_loup(tabloup); // distance loup P ou L
        etat[k+1].dis = longueur;
    }
}


int main()
{
    emplacement_t tabloup[2];
    emplacement_t tabproie[3];
    observation_t etat[4];

    tabloup[0].x = 3; tabloup[0].y = 9;
    tabloup[1].x = 8; tabloup[1].y = 8;

    tabproie[0].x = 2; tabproie[0].y = 10;
    tabproie[1].x = 15; tabproie[1].y = 18;
    tabproie[2].x = 15; tabproie[2].y = 2;

    etat_loup(tabloup, tabproie, etat);
    printf("\n");
    for(int k = 0; k < 4; k++)
    {
        printf("%d %d \n", etat[k].pos, etat[k].dis);
    }
    printf("\n");
    return 0;

}


