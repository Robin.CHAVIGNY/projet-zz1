#include <stdio.h>
#include<string.h>

#define NB_REGLE 8

typedef enum { //enum qui definie la position
    JOKER1 =-1,N, O, S, E
} position_t;

typedef enum { //enum qui definie la distance
    JOKER2 =-1, P, L //proche <7 et loin >=7
} distance_t;

typedef struct { //structure qui donne l'observation d'un proie ou d'un predateur (poistion+distance)
    position_t pos;
    distance_t dis;
} observation_t;

typedef enum { //enum qui definie l'action à réaliser
    NN, OO, SS, EE
} action_t;

typedef struct { // structure qui définie une règle
    observation_t proie;
    observation_t predateur;
    action_t action;
    int priorite;
} Regle;

typedef struct{ //structure qui définie le tableau de règles
    Regle *tab;
}TabRegle;

typedef struct{ //structure qui donne l'emplacement du loup
    int x;
    int y;
}emplacement_t;


int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup);
int position( emplacement_t loup, emplacement_t proie);
int distance_proie(emplacement_t loup, emplacement_t proie);
int loup_le_plus_proche(emplacement_t loup[], int k);
int distance_loup(emplacement_t tabloup[]);
void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[]);






