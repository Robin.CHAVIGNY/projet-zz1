#include <SDL2/SDL.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>
#include <time.h>
#include "proba.h"


int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup)
{
    int indice=0;
    int min_dist= sqrt((loup.x-proie[0].x)*(loup.x-proie[0].x)+(loup.y-proie[0].y)*(loup.y-proie[0].y));
    for(int k=1; k<3;k++)
    {
        int distance=sqrt((loup.x-proie[k].x)*(loup.x-proie[k].x)+(loup.y-proie[k].y)*(loup.y-proie[k].y));
        if(distance<min_dist)
        {
            min_dist=distance;
            indice=k;
        }
    }
    return indice;
}

int position( emplacement_t loup, emplacement_t proie)
{
    int resultat;
    float i=(float)rand() / (float)RAND_MAX;
    if (loup.x>proie.x)
    {
        if(loup.y>proie.y)
        {
            if(i<0.5)
            {
                resultat=1;
            }
            else
                resultat=0;             
        }
        if(loup.y<=proie.y)
        {
            if(i<=0.5)
            {
                resultat=2;
            }
            else
            {
                resultat=1;
            } 
        }

    }  
    else
    {
        if(loup.y>proie.y)
        {
            if(i<=0.5)
            {
                resultat=0;
            }
            else
                resultat=3;
                    
        }
        if(loup.y<=proie.y)
        {
            if(i<=0.5)
            {
                resultat=3;
            }
            else
            {
                resultat=2;
            }            
        }
    }
    return resultat; 
}

int distance_proie(emplacement_t loup, emplacement_t proie)
{
    int resultat;
    int distance = sqrt((loup.x-proie.x)*(loup.x-proie.x)-(loup.y-proie.y)*(loup.y-proie.y));
    if(distance<7)
    {
        resultat=0;
    }
    else
        resultat=1;

    return resultat;
}

int loup_le_plus_proche(emplacement_t loup[], int k)
{
    int resultat;
    if (k==0)
    {
        resultat = position(loup[0], loup[1]);
    }
    else
        resultat = position(loup[1], loup[0]);

    return resultat;
}

int distance_loup(emplacement_t tabloup[])
{
    int resultat;
    resultat=distance_proie(tabloup[0], tabloup[1]);
    return resultat;
}

void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[],int nb_proie)
{
    {
        int indice=proie_la_plus_proche(tabproie, tabloup[0],nb_proie);  //recherche de la proie la plus 
        int direction = position(tabloup[0], tabproie[indice]); //position proie N,S,E,O
        etat[0].pos = direction;
        int longueur = distance_proie(tabloup[0], tabproie[indice]); // distance de la proie P, L
        etat[0].dis=longueur;

        int loup=loup_le_plus_proche(tabloup, 0); // position loup N,S,E,O
        etat[1].pos=loup;
        longueur = distance_loup(tabloup); // distance loup P ou L
        etat[1].dis=longueur;


        int indice2=proie_la_plus_proche(tabproie, tabloup[1],nb_proie);  //recherche de la proie la plus 
        int direction2 = position(tabloup[1], tabproie[indice2]); //position proie N,S,E,O
        etat[2].pos = direction2;
        int longueur2 = distance_proie(tabloup[1], tabproie[indice2]); // distance de la proie P, L
        etat[2].dis=longueur2;

        int loup2=loup_le_plus_proche(tabloup, 1); // position loup N,S,E,O
        etat[3].pos=loup2;
        longueur2 = distance_loup(tabloup); // distance loup P ou L
        etat[3].dis=longueur2;
    }
}

double somme_calcul_proba_event(TabRegle tableau, int s, int regle_accepte[])
{
    double x=0;
    for (int i = 0; i < nb_regle ; i++)
    {
        if (regle_accepte[i]==1)
        {
            x = x + pow(tableau.tab[i].priorite + 1, s);
        }
    }
    return x;
}

void calcul_proba_event(TabRegle tableau, float s, double x, double L[], int regle_accepte[])
{
    float S=0;
    float tmp;
    for (int i = 0; i < nb_regle ; i++)
    {
        if(regle_accepte[i]==1)
        {
            tmp= pow(tableau.tab[i].priorite + 1, s)/x; 
            S = S + tmp;
            L[i]=S;
        }
    }
}

int choix_proba(double L[], float nombre)
{
    int resultat=0;
    while(L[resultat]<nombre)
    {
            resultat=resultat +1;
    }
    return resultat;
}

Regle chargerRegle(FILE* flux)
{
    Regle regle;
    fscanf(flux, " - [%d, %d, %d, %d] --> %d (%d)",
                    &regle.proie.pos,
                    &regle.proie.dis,
                    &regle.predateur.pos,
                    &regle.predateur.dis,
                    &regle.action,
                    &regle.priorite);
    return regle;
}

TabRegle chargerTabRegle(FILE* flux, int N)
{
    TabRegle tabRegle;
    for(int k = 0; k < N; k++)
    {
        Regle regle;
        regle = chargerRegle(flux);
        tabRegle.tab[k] = regle;
    }
    return tabRegle;
}

void afficherRegle(FILE* flux, Regle regle)
{
    fprintf(flux, "- [%d, %d, %d, %d] --> %d (%d)\n",
            regle.proie.pos, regle.proie.dis,
            regle.predateur.pos, regle.predateur.dis,
            regle.action, regle.priorite);
}

void afficherTabRegle(FILE* flux, TabRegle tabRegle)
{
    for(int k = 0; k < nb_regle; k++)
    {
        afficherRegle(flux, tabRegle.tab[k]);
    }
}

void choix_regles(observation_t loup1, observation_t loup2, int regle_accepte[], TabRegle tabregle)
{
    for (int k=0; k<nb_regle; k++)
    {
    if((loup1.pos==tabregle.tab[k].proie.pos)||(tabregle.tab[k].proie.pos==-1))
    {
        if((tabregle.tab[k].proie.dis==-1)||(loup1.dis==tabregle.tab[k].proie.dis))
        {
            if((loup2.pos==tabregle.tab[k].predateur.pos)||(tabregle.tab[k].predateur.pos==-1))
            {
                if((loup2.dis==tabregle.tab[k].predateur.dis)||(tabregle.tab[k].predateur.dis==-1))
                {
                    regle_accepte[k]=1;
                }
            }
        }
    }
    }
}


int main()
{
    TabRegle tabregle;
    FILE* flux = fopen("./regle.txt", "r");
    tabregle = chargerTabRegle(flux, nb_regle);
    afficherTabRegle(stdout, tabregle);
    printf("\n");

    float nombre = 0;

    srand(time(NULL));
    nombre = (float)rand() / (float)RAND_MAX;

    int regle_accepte[nb_regle];

    emplacement_t tabloup[2];
    emplacement_t tabproie[3];
    observation_t etat[4];

    tabloup[0].x=3; tabloup[0].y = 9;
    tabloup[1].x=8; tabloup[1].y = 8;

    tabproie[0].x=2; tabproie[0].y=10;
    tabproie[1].x=15; tabproie[1].y=18;
    tabproie[2].x=15; tabproie[2].y=2;

    etat_loup(tabloup, tabproie, etat, nb_proie);
    /*printf("%d %d %d %d\n %d %d %d %d\n",
        etat[0].pos, etat[0].dis, etat[1].pos,
        etat[1].dis, etat[2].pos, etat[2].dis,
        etat[3].pos, etat[3].dis);*/

    for(int k=0; k<4;k=k+2)
    {
        double x=0;
        for(int k=0;k<nb_regle; k++)
        {
            regle_accepte[k]=0;
        }

        choix_regles(etat[k], etat[k+1], regle_accepte, tabregle);

        printf(" les règle choisies sont : ");
        for(int k=0;k<nb_regle; k++)
        {
            printf("%d", regle_accepte[k]);
         }
        printf("\n");

        x = somme_calcul_proba_event(tabregle, 1, regle_accepte);

        printf("%f\n", x);

        double L[nb_regle];
        calcul_proba_event(tabregle, 1, x, L, regle_accepte);
        for(int k=0;k<nb_regle; k++)
        {
            printf("%f ", L[k]);
        }

        printf("\n");

        int indice = choix_proba(L, nombre);

        printf("%f %d\n", nombre, indice);

        printf("\n");

        printf("%d %d %d %d -> %d", tabregle.tab[indice].proie.pos,
                tabregle.tab[indice].proie.dis,
                tabregle.tab[indice].predateur.pos,
                tabregle.tab[indice].predateur.dis,
                tabregle.tab[indice].action);
        printf("\n");
    }

    fclose(flux);

    return 0;
}
