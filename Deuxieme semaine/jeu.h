#include <SDL2/SDL.h>
#include <stdio.h>
#include<string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdbool.h>
#include <threads.h>

/* Taille de la fenêtre */
#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

#define T 30
#define nb_regle 30

#define NB_LOUP 11

typedef enum 
{
    JOKER1 =-1,N, O, S, E
} position_t;

typedef enum {
    JOKER2 =-1, P, L
} distance_t;

typedef struct {
    position_t pos;
    distance_t dis;
} observation_t;


typedef enum {
    NN, OO, SS, EE
} action_t;

typedef struct{
    int x;
    int y;
}emplacement_t;

typedef struct {
    observation_t proie;
    observation_t predateur;
    action_t action;
    int priorite;
} Regle;

typedef struct{
    Regle *tab;
}TabRegle;

void afficherRegle(FILE* flux, Regle regle);

Regle chargerRegle(FILE* flux);
TabRegle chargerTabRegle(FILE* flux);
void afficherTabRegle(FILE* flux, const TabRegle tabRegle);
void freeTabregle(TabRegle tabregle);


void background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer);

int **creat_world ();

void free_world(int** matrice);

void genere_labyrinthe (int** matrice, int bas, int haut);

void printMatrice(int** matrice);

void loc_proie(int **matrice);
void loc_predateur(int **matrice);

void afficher_proie(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window);
void afficher_predateur(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window);
void afficher_kill(SDL_Texture *sprite, SDL_Renderer *renderer, int **world,SDL_Window *window);

int nb_bon_voisin(int **world, int x, int y);

void deplacer_proie(int **world, int nb_proie);

void deplacer_predateur(int **world,TabRegle tabregle,int nb_proie);

int proie_kill (int **world);

void clean_world (int **world);

//fonction pour rendre le loup conscient 
int proie_la_plus_proche(emplacement_t proie[], emplacement_t loup,int nb_proie);
int position( emplacement_t loup, emplacement_t proie);
int distance_proie(emplacement_t loup, emplacement_t proie);
int loup_le_plus_proche(emplacement_t loup[], int k);
int distance_loup(emplacement_t tabloup[]);
void etat_loup(emplacement_t tabloup[], emplacement_t tabproie[], observation_t etat[],int nb_proie);
void move_predateur(emplacement_t tabloup[],emplacement_t tabproie[], int M[],TabRegle tabregle, int nb_proie);

int IA(void *tabregle);

float moyenne_thread(TabRegle tabregle);

TabRegle changement_regle(FILE* flux);

int moyenne_IA(void* tabregle_void);