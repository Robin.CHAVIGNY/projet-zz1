#include "pacman.h"
#include <time.h>

int main()
{    
    srand(time(NULL));
    SDL_Texture *sprite;
    SDL_Texture* diamond_texture;
    SDL_Texture *my_texture;
    SDL_Window *window;
    SDL_bool program_on = SDL_TRUE;                        // Booléen pour dire que le programme doit continuer
    SDL_Event event; 
    SDL_Renderer *renderer;
    SDL_Texture *score_texture;
    SDL_Texture *win_texture;
    SDL_Rect rect_score = {0};
    SDL_RendererFlip flip;

    float angle = 0;
    int score = 0;
    int hasIntersectDiamond = 0;

    window = SDL_CreateWindow("PacMan",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,1000,1000,SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",SDL_GetError()); 
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        
    if (renderer == NULL)
    {
        SDL_Log("Error : Renderer - %s\n", SDL_GetError()); 
    }

    sprite = IMG_LoadTexture(renderer, "pac2.xcf");

    if (sprite == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }

    my_texture = IMG_LoadTexture(renderer, "background.png");


    if (my_texture == NULL)
    {
        SDL_Log("Error : Erreur lors de l'ouverture de l'image - %s\n",SDL_GetError()); 
    }

    diamond_texture = IMG_LoadTexture(renderer,"listDiamond.png");
    
  	if (diamond_texture == NULL) 
  		fprintf(stderr, "Erreur d'initialisation de la texture : %s\n", SDL_GetError());

    int x = rand()%800;
    int y = 80 + rand()%640;

    SDL_Rect a = {0, 80, 45.5, 43.75};


    SDL_Rect position_diamant = {x, y, 182, 175};

    SDL_Rect nv_diamant = {x, y, position_diamant.w*0.25, position_diamant.h*0.25};


    SDL_RenderClear(renderer);
    background(my_texture,window,renderer);
    SDL_Rect tilerect[2] = {{0,0,91,92},{0,92,91,92}};

    SDL_RenderCopy(renderer, sprite, &tilerect[1], &a);

    while (program_on == SDL_TRUE)
    {
                // La boucle des évènements
        while (SDL_PollEvent(&event))
        {                                                   // Tant que on n'a pas trouvé d'évènement utile                                    // et la file des évènements stockés n'est pas vide et qu'on n'a pas                                             // terminé le programme Défiler l'élément en tête de file dans 'event'
            switch (event.type)
            {                                               // En fonction de la valeur du type de cet évènement
                case SDL_QUIT:                              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                    program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
                    break;

                case SDL_KEYDOWN:                           // Le type de event est : une touche appuyée
                                                            // comme la valeur du type est SDL_Keydown, dans la partie 'union' de
                                                            // l'event, plusieurs champs deviennent pertinents   
                    switch (event.key.keysym.sym)
                    {           
          
                        case SDLK_SPACE:                            
                            program_on = SDL_FALSE;                           // 'escape' ou 'q', d'autres façons de quitter le programme                     
                            break;
                    }
                default:
                    break;
            }
            SDL_RenderClear(renderer);
            background(my_texture,window,renderer);
            a = handleMovementEvent(event, a, a, renderer, sprite, &angle);
            SDL_RenderCopyEx(renderer, sprite, &tilerect[1], &a, angle, NULL, SDL_FLIP_NONE );
        }
        printf("rect %d %d\n", a.x, a.y);
        a.x = a.x%1000;
        if (a.x < 0) 
        {
            a.x += 1000;
        }
        if (a.y < 80)
        {
            a.y += 920;
        }
        if (a.y > 1000)
        {
            a.y-=920;
        }

    if (TTF_Init() < 0) 
    {
        SDL_Log("Error : Couldn't initialize SDL TTF - %s\n", SDL_GetError());
    }
    
    TTF_Font* font = NULL;
    TTF_Font *font2 = NULL;
    TTF_Font *font3 = NULL;

                                       
    font = TTF_OpenFont("ARCADE_R.TTF", 25);
                         // La police à charger, la taille désirée
    if (font == NULL)
    {
        SDL_Log("Error : Can't load font - %s\n", SDL_GetError());
    }

    font2 = TTF_OpenFont("ARCADE_R.TTF",15);
                         // La police à charger, la taille désirée
    if (font2 == NULL)
    {
        SDL_Log("Error : Can't load font - %s\n", SDL_GetError());
    }

    font3 = TTF_OpenFont("ARCADE_R.TTF",150);
                         // La police à charger, la taille désirée
    if (font3 == NULL)
    {
        SDL_Log("Error : Can't load font - %s\n", SDL_GetError());
    }

    TTF_SetFontStyle(font, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);
    TTF_SetFontStyle(font2, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);
    TTF_SetFontStyle(font3, TTF_STYLE_ITALIC | TTF_STYLE_BOLD);

             // en italique, gras

    SDL_Color color = {30, 0, 40, 255};                                  // la couleur du texte
    SDL_Surface* text_surface = NULL;
    SDL_Surface* text_surface2 = NULL;

                                      // la surface  (uniquement transitoire)
                                       // la surface  (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, "Bienvenue sur le jeu Pac Man!", color); // création du texte dans la surface 
    if (text_surface == NULL)
    {
        SDL_Log("Error : Can't create text surface - %s\n", SDL_GetError());
    }

    if (score == 0)
    {
        text_surface2 = TTF_RenderText_Blended(font2, "Score : 0", color); // création du texte dans la surface 
        if (text_surface2 == NULL)
        {
            SDL_Log("Error : Can't create text surface - %s\n", SDL_GetError());
        }
    }
        
    if (score == 1)
    {
        text_surface2 = TTF_RenderText_Blended(font2,"Score : 1", color); // création du texte dans la surface 
        if (text_surface2 == NULL)
        {
            SDL_Log("Error : Can't create text surface - %s\n", SDL_GetError());
        }
    }

    if (score == 2)
    {
        text_surface2 = TTF_RenderText_Blended(font2, "Score : 2", color); // création du texte dans la surface 
        if (text_surface2 == NULL)
        {
            SDL_Log("Error : Can't create text surface - %s\n", SDL_GetError());
        }
    }

    if (score == 3)
    {
        text_surface2 = TTF_RenderText_Blended(font2, "Score : 3", color); // création du texte dans la surface 
        if (text_surface2 == NULL)
        {
            SDL_Log("Error : Can't create text surface - %s\n", SDL_GetError());
        }
    }
    SDL_Texture* text_texture = NULL; 
    SDL_Texture* score_texture = NULL;                            // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); 
    score_texture = SDL_CreateTextureFromSurface(renderer, text_surface2);
    // transfert de la surface à la texture
    
    if (text_texture == NULL) 
    {
        SDL_Log("Error : Can't create texture from surface - %s\n", SDL_GetError());
    }

    if (score_texture == NULL) 
    {
        SDL_Log("Error : Can't create texture from surface - %s\n", SDL_GetError());
    }

    SDL_FreeSurface(text_surface);
    SDL_FreeSurface(text_surface2);

    SDL_Rect pos = {150, 15, 0, 0};    
    SDL_Rect pos2 = {825, 75, 0, 0};   

                                         // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);
    SDL_QueryTexture(score_texture, NULL, NULL, &pos2.w, &pos2.h);         // récupération de la taille (w, h) du texte 
                                                              // récupération de la taille (w, h) du texte 
    SDL_RenderCopy(renderer, text_texture, NULL, &pos);
    SDL_RenderCopy(renderer, score_texture, NULL, &pos2);                  // Ecriture du texte dans le renderer   
                      // Ecriture du texte dans le renderer   
    SDL_DestroyTexture(text_texture);
    SDL_DestroyTexture(score_texture);   
    
    hasIntersectDiamond = SDL_HasIntersection(&a, &nv_diamant) ==  SDL_TRUE ?  1 : 0;

    	if(hasIntersectDiamond) // si on a touché un diamant on en ajoute un nouveau ailleurs
        {
            nv_diamant.x = rand()%800;
            nv_diamant.y = 80 + rand()%340;
            score++;
            hasIntersectDiamond = 0;
            SDL_RenderCopy(renderer, sprite, &tilerect[0], &a);
            SDL_RenderPresent(renderer);
            SDL_Delay(10);
        }
        else
        {
            create_diamond(renderer,diamond_texture, &nv_diamant);
            SDL_Delay(10);
        }
        create_diamond (renderer, diamond_texture, &nv_diamant);                                // On n'a plus besoin de la texture avec le texte

    SDL_RenderPresent(renderer);


    if (score == 3)
        {
            program_on = SDL_FALSE; // Il est temps d'arrêter le programme
            SDL_RenderClear(renderer);
            background(my_texture,window,renderer);
            SDL_Surface* text_surface3 = NULL;
            SDL_Texture* win_texture = NULL;     
            text_surface3 = TTF_RenderText_Blended(font, "Victoire !", color); 
            win_texture = SDL_CreateTextureFromSurface(renderer, text_surface3);
            if (win_texture == NULL) 
            {
                SDL_Log("Error : Can't create texture from surface victory- %s\n", SDL_GetError());
            }
            SDL_FreeSurface(text_surface3);
            SDL_Rect pos3 = {360, 450, 0, 0};
            SDL_QueryTexture(win_texture, NULL, NULL, &pos3.w, &pos3.h);
            SDL_RenderCopy(renderer, win_texture, NULL, &pos3);
            SDL_RenderPresent(renderer); 
            SDL_DestroyTexture(win_texture);                  // Ecriture du texte dans le renderer   
                    // récupération de la taille (w, h) du texte 
            SDL_Delay(5000);
        }   

    }
    
    TTF_Quit();
    
    SDL_DestroyTexture(my_texture);
    SDL_DestroyTexture(sprite);
    SDL_DestroyTexture(diamond_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
    return 0;
}



