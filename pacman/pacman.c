#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include "pacman.h"



void background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Rect
        source = {0},
        window_dimensions = {0},
        destination = {0};
    
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

/*
void animation(SDL_Texture *my_texture,SDL_Event event, SDL_Rect a, SDL_Texture *sprite, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect destination_ecran)
{

    //SDL_Rect tilerect[2] = {{0,0,91,92},{0,92,91,92}};

    handleMovementEvent(event, destination_ecran, a, renderer, sprite);

    //SDL_RenderClear(renderer);
    //background(my_texture,window,renderer);

    //SDL_RenderCopy(renderer, sprite, &tilerect[1], &destination_ecran);
    
}*/

void moveCharacter(SDL_Texture *sprite, SDL_Renderer *renderer, SDL_Rect *a)
{

    SDL_Rect tilerect[2] = {
        {0, 0 ,91, 92},
        {0, 92, 91, 92}
    };	

    SDL_RenderCopy(renderer, sprite, &tilerect[1], a);

}


void create_diamond(SDL_Renderer* renderer, SDL_Texture*diamond_texture, SDL_Rect *a)       
{

    SDL_Rect tilerect2 = {0, 0 , 185, 175};

    SDL_RenderCopy(renderer, diamond_texture, &tilerect2, a);
}



SDL_Rect handleMovementEvent(SDL_Event event, SDL_Rect destination_ecran, SDL_Rect a, SDL_Renderer *renderer, SDL_Texture *sprite, float *angle)
{
    int x = 40;

    SDL_Rect tilerect[2] = {
        {0, 0 ,91, 92},
        {0, 92, 91, 92}
    };



	switch(event.type)
	{
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym)
			{

				case SDLK_UP:
					destination_ecran.y += -x;
                    //SDL_RenderCopyEx(renderer, sprite, &tilerect[1], &a, -90, NULL, SDL_FLIP_NONE);
                    *angle = -90;
					break;

				case SDLK_DOWN:
					destination_ecran.y += x;
                    //SDL_RenderCopyEx(renderer, sprite, &tilerect[1], &a, 90, NULL, SDL_FLIP_NONE);
                    *angle = 90;
					break;

				case SDLK_LEFT:
					destination_ecran.x += -x;
                    //SDL_RenderCopyEx(renderer, sprite, &tilerect[1], &a, -180, NULL, SDL_FLIP_NONE);
                    *angle = -180;
			        break;

				case SDLK_RIGHT:
					destination_ecran.x += x;
                    //SDL_RenderCopyEx(renderer, sprite, &tilerect[1], &a, 0, NULL, SDL_FLIP_NONE);
                    *angle = 0;
                    break;

				default:
					break;
			}
			break;

		default:
			break;
	}
    
    return destination_ecran;
}

