#include <SDL2/SDL.h>
#include <stdio.h>
#include<string.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

/* Taille de la fenêtre */
#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

/* Taille d'un sprite */
#define SPRITE_SIZE 20


/* Le sprite contient 2 images pour chaque position
Ordre de chaque position dans le sprite */
#define DIR_UP          0
#define DIR_RIGHT       1
#define DIR_DOWN        2
#define DIR_LEFT        3

/* Nombre de pixels pour chaque pas du personnage */
#define SPRITE_STEP 6

void background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer);

//void animation(SDL_Texture *my_texture, SDL_Texture *sprite, SDL_Window *window, SDL_Renderer *renderer, SDL_Rect destination_ecran);

void moveCharacter(SDL_Texture *sprite, SDL_Renderer *renderer, SDL_Rect *a);

SDL_Rect handleMovementEvent(SDL_Event event,  SDL_Rect destination_ecran, SDL_Rect a, SDL_Renderer *renderer, SDL_Texture *sprite, float *angle);

void create_diamond(SDL_Renderer* renderer, SDL_Texture *diamond_texture, SDL_Rect *a);